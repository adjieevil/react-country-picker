import axios from 'axios';

export const fetchProject = (errorHandler) => {
    try {
        const user = axios.get('https://vercel.com/api/v2/projects/?limit=8&latestDeployments=3', {
            headers: {
              'Authorization': `Bearer 6P87T3hMJ8kQvHdaN4iGvKwY`
            }
          } )
          return user
    } catch(error){
        errorHandler(error)
    }

    try {
      const user = axios.get('https://restcountries.eu/rest/v2/all', {

        } )
        return user
    } catch(error){
        errorHandler(error)
    }
}

export const fetchCountry = (errorHandler) => {
  try {
    const user = axios.get('https://restcountries.eu/rest/v2/all', {

      } )
      return user
  } catch(error){
      errorHandler(error)
  }
}

export const fetchCountryName = (errorHandler, name) => {
  try {
    const user = axios.get('https://restcountries.eu/rest/v2/name/' + name, {

      } )
      return user
  } catch(error){
      errorHandler(error)
  }
}

export const fetchCountryRegion = (errorHandler, region) => {
  try {
    const user = axios.get('https://restcountries.eu/rest/v2/region/' + region, {

      } )
      return user
  } catch(error){
      errorHandler(error)
  }
}