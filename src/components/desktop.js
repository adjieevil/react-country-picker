import React, {useEffect, useState} from 'react';
import { fetchCountry, fetchCountryName, fetchCountryRegion } from '../services/'


function Desktop() {
    const [isCountry, setCountry] = useState([]);
    const [isMode, setMode] = useState(false);

    async function getCountry() {
        const country = await fetchCountry(error => console.error(error))
        let data = country.data
        console.log(data);
        setCountry(data);
    }

    async function handleInputChange(event) {
        setCountry([]);
        const name = event.currentTarget.value;
        if (name === '') {
            getCountry()
            return
        }
        const country = await fetchCountryName(error => console.error(error), name)
        let data = country.data
        setCountry(data);
    };

    async function handleSelectChange(event) {
        setCountry([]);
        const region = event.target.value
        if (region === '') {
            getCountry()
            return
        }
        const country = await fetchCountryRegion(error => console.error(error), region)
        let data = country.data
        setCountry(data)
    }

    function handleIsMode() {
        setMode(!isMode)
    }

    useEffect( () => {
        getCountry();
      }, []);

    return (
        <>
        {isMode ? 
        <div className="h-screen overflow-auto bg-dark-custom-1 w-full">
            <div className="flex flex-col w-full">
                <div class="flex justify-between bg-dark-custom-2 w-full">
                    <div class="text-gray-700 text-center px-4 py-2 m-2 mx-12 lg:mx-8">
                        <div class="flex flex-row">
                            <div class="text-white text-lg text-center font-semibold">Where in the World?</div>
                        </div>
                    </div>
                    <div class="text-gray-700 text-center px-4 py-2 m-2 mx-12 lg:mx-8">
                        {isMode ? 
                        <div onClick={handleIsMode} class="flex flex-row cursor-pointer">
                            <div class="text-gray-700 px-1 text-center">🌑</div>
                            <div class="text-white font-semibold text-center">Dark Mode</div>
                        </div> :
                        <div onClick={handleIsMode} class="flex flex-row cursor-pointer">
                            <div class="text-gray-700 px-1 text-center">☀️</div>
                            <div class="text-gray-700 font-semibold text-center">Light Mode</div>
                        </div>
                        }
                    </div>
                </div>
                <div class="bg-dark-custom-1 my-4 mx-4">
                    <div class="flex justify-between w-full my-4 lg:my-2">
                        <div class="text-white text-center px-4 py-2 m-2 mx-8 lg:mx-4 w-3/12">
                            <div class="flex flex-row bg-dark-custom-2 rounded-md px-2 py-2 w-full shadow-lg">
                                <div class="text-white text-xl text-center px-2 pl-3 py-1">🔍</div>
                                <input onChange={handleInputChange} class="appearance-none bg-dark-custom-2 rounded w-full px-3 py-3 text-white text-sm leading-tight focus:outline-none focus:shadow-outline" id="username" type="text" placeholder="Search for a country..."></input>
                            </div>
                        </div>
                        <div class="text-gray-700 text-center px-4 py-2 m-2 mx-8 lg:mx-4">
                            <div class="flex flex-row bg-dark-custom-2 rounded-md py-2 px-2 shadow-lg">
                                <div class="relative">
                                    <select onChange={handleSelectChange} class="block appearance-none w-full text-white bg-dark-custom-2 text-sm tracking-wide py-3 px-2 pr-10 rounded leading-tight focus:outline-none focus:bg-dark-custom-2 focus:border-gray-500" id="grid-state">
                                        <option value="">Filter by Region</option>
                                        <option value="Africa">Africa</option>
                                        <option value="Americas">Americas</option>
                                        <option value="Asia">Asia</option>
                                        <option value="Europe">Europe</option>
                                        <option value="Oceania">Oceania</option>
                                    </select>
                                    <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                        <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="flex flex-col my-4 mx-2">
                        <div class="mx-10 lg:mx-6 text-gray-300 text-sm">About {isCountry.length} result countries</div>
                        <div class="flex flex-wrap">
                            {isCountry.map((value, index) => {
                                return <div key={index} class="w-3/12 ">
                                    <div class="flex-col bg-white mx-10 lg:mx-6 my-4 shadow-md">
                                        <img class="h-64 lg:h-48" src={value.flag}></img>
                                        <div class="flex-col bg-dark-custom-2 px-6 pb-10 py-4">
                                            <div class="text-white text-xl tracking-wide font-semibold py-2 px-1">{value.name}</div>
                                            <div class="text-white px-1"><strong>Population: </strong>{value.population}</div>
                                            <div class="text-white px-1"><strong>Region: </strong>{value.region}</div>
                                            <div class="text-white px-1"><strong>Capital: </strong>{value.capital}</div>
                                        </div>

                                    </div>
                            </div>
                            })}
                        </div>
                    </div>
                </div>
            </div>
        </div> :
        <div className="h-screen overflow-auto bg-custom-1 w-full">
            <div className="flex flex-col w-full">
                <div class="flex justify-between bg-white w-full">
                    <div class="text-gray-700 text-center px-4 py-2 m-2 mx-12 lg:mx-8">
                        <div class="flex flex-row">
                            <div class="text-gray-700 text-lg text-center font-semibold">Where in the World?</div>
                        </div>
                    </div>
                    <div class="text-gray-700 text-center px-4 py-2 m-2 mx-12 lg:mx-8">
                        {isMode ? 
                        <div onClick={handleIsMode} class="flex flex-row cursor-pointer">
                            <div class="text-gray-700 px-1 text-center">🌑</div>
                            <div class="text-gray-700 font-semibold text-center">Dark Mode</div>
                        </div> :
                        <div onClick={handleIsMode} class="flex flex-row cursor-pointer">
                            <div class="text-gray-700 px-1 text-center">☀️</div>
                            <div class="text-gray-700 font-semibold text-center">Light Mode</div>
                        </div>
                        }
                    </div>
                </div>
                <div class="bg-custom-1 my-4 mx-4">
                    <div class="flex justify-between w-full my-4 lg:my-2">
                        <div class="text-gray-700 text-center px-4 py-2 m-2 mx-8 lg:mx-4 w-3/12">
                            <div class="flex flex-row bg-white rounded-md px-2 py-2 w-full shadow-lg">
                                <div class="text-gray-700 text-xl text-center px-2 pl-3 py-1">🔍</div>
                                <input onChange={handleInputChange} class="appearance-none bg-white rounded w-full px-3 py-3 text-gray-700 text-sm leading-tight focus:outline-none focus:shadow-outline" id="username" type="text" placeholder="Search for a country..."></input>
                            </div>
                        </div>
                        <div class="text-gray-700 text-center px-4 py-2 m-2 mx-8 lg:mx-4">
                            <div class="flex flex-row bg-white rounded-md py-2 px-2 shadow-lg">
                                <div class="relative">
                                    <select onChange={handleSelectChange} class="block appearance-none w-full text-gray-700 text-sm tracking-wide py-3 px-2 pr-10 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-state">
                                        <option value="">Filter by Region</option>
                                        <option value="Africa">Africa</option>
                                        <option value="Americas">Americas</option>
                                        <option value="Asia">Asia</option>
                                        <option value="Europe">Europe</option>
                                        <option value="Oceania">Oceania</option>
                                    </select>
                                    <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                        <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="flex flex-col my-4 mx-2">
                        <div class="mx-10 lg:mx-6 text-gray-700 text-sm">About {isCountry.length} result countries</div>
                        <div class="flex flex-wrap">
                            {isCountry.map((value, index) => {
                                return <div key={index} class="w-3/12 ">
                                    <div class="flex-col bg-white mx-10 lg:mx-6 my-4 shadow-md">
                                        <img class="h-64 lg:h-48" src={value.flag}></img>
                                        <div class="flex-col px-6 pb-10 py-4">
                                            <div class="text-gray-700 text-xl tracking-wide font-semibold bg-white py-2 px-1">{value.name}</div>
                                            <div class="text-gray-700 bg-white px-1"><strong>Population: </strong>{value.population}</div>
                                            <div class="text-gray-700 bg-white px-1"><strong>Region: </strong>{value.region}</div>
                                            <div class="text-gray-700 bg-white px-1"><strong>Capital: </strong>{value.capital}</div>
                                        </div>

                                    </div>
                            </div>
                            })}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    }
        </>
    );
}

export default Desktop;