import React, {useEffect, useState} from 'react';
import Ane50 from '../images/anee.jpg';
import Instagram from '../images/instagram.png';
import Linkedin from '../images/linkedin.png';
import Medium from '../images/medium.png';
import Github from '../images/github.png';
import Gitlab from '../images/gitlab.png';

function Mobile({projects}) { 
    return (
        <>
        <div className="flex flex-col my-4">
            <div className="flex flex-col shadow-xl border-4 border-black m-2 mr-3">
                <div class="flex flex-row mt-6">
                    <div className="w-2/6">
                        <div className="text-gray-700 text-center pl-2 pr-1">
                            <img className="w-28 rounded-full border-3 border-black" src={Ane50}></img>
                        </div>
                    </div>
                    <div className="w-4/6">
                        <div className="flex flex-col mx-2">
                            <div className="text-gray-700 text-xl tracking-wide font-medium font-sans m-2 ">Adjie Wijaya Kusuma</div>
                            <div className="text-custom-5 text-base italic m-2">Nuxt.js - Gatsby - SSR - SSG - PWA 💻 Books 📖 Sport 🏃‍♂️ Nature 🌱 Focus on being useful.  In my spare time, I fancy myself writing a story book.</div>
                            <div className="flex flex-row">
                                <a href='https://www.instagram.com/adjie214/' className="text-gray-700 text-center px-2 py-1 my-2">
                                    <img className="w-6" src={Instagram}></img>
                                </a>
                                <a href='https://www.linkedin.com/in/adjie214/' className="text-gray-700 text-center px-2 py-1 m-2">
                                    <img className="w-6" src={Linkedin}></img>
                                </a>
                                <a href='https://medium.com/@Adjie214' className="text-gray-700 text-center py-1 m-2">
                                    <img className="w-6" src={Medium}></img>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="text-gray-700 ml-3 mb-4">
                    <div className="flex flex-col font-desc my-2">
                        <div className="text-gray-700 px-3 my-1 mx-2">📧 Email</div>
                        <div className="text-gray-700 text-base px-3 my-1 mx-2">Adjieevil@gmail.com</div>
                    </div>
                    <div className="flex flex-col font-desc my-2">
                        <div className="text-gray-700 px-3 my-1 mx-2">📍 Location</div>
                        <div className="text-gray-700 text-base px-3 my-1 mx-2">Jakarta, Indonesia</div>
                    </div>
                    <div className="flex flex-col font-desc my-2">
                        <div className="text-gray-700 px-3 my-1 mx-2">📧 work</div>
                        <div className="text-gray-700 text-base px-3 my-1 mx-2">I'm working at a computer and scolded every day !! Kidding :)</div>
                    </div>
                </div>
            </div>
            <div className="my-4">
            {projects !== null ? projects.map(function(project, index){
                var projectName = project.link.projectNamespace || project.link.org
                return <div className="w-full border-t border-custom-4 last:border-b" key={index}>
                <div className=" text-gray-700 bg-white m-2 ">
                    <div className="flex justify-between px-2 pt-2">
                        <div className="self-center text-gray-700 text-center text-lg capitalize font-bold tracking-wide truncate px-4 py-2">🗄️ {project.link.projectName || project.link.repo}</div>
                        <div className="flex justify-start content-center">
                            <div className="text-gray-700 text-xs text-center bg-custom-1 font-condensed border-2 border-black font-extrabold uppercase rounded-md px-3 py-1 m-2 mx-1">
                                <a href={'https://' + project.targets.production.url} target="blank">Demo</a>
                            </div>
                            <div className="text-gray-700 text-xs text-center bg-custom-2 font-condensed border-2 border-custom-3 font-extrabold uppercase rounded-md px-3 py-1 m-2 mx-1">
                                <a href={project.link.projectUrl || 'https://github.com/' + projectName  + '/' + project.name} target="blank">Visit</a>
                            </div>
                        </div>
                    </div>
                    <div className="flex flex-col px-3">
                        <div className="flex flex-row text-sm font-desc font-semibold mx-4 ">
                            {project.tag.map(function(x, index){
                                return <div className="text-gray-700 text-sm text-center px-1">{x}</div>
                            })} 
                        </div>
                        <div className="text-gray-700 text-xsm px-5 font-medium font-desc py-2">Hello its me Use .flex-initial to allow a flex item to shrink but not grow</div>    
                        <hr className=" border-t-2 border-custom-4 mt-1"/>
                        <div className="text-gray-700 px-3 py-1 mx-2 my-1">
                            <img className="inline w-5 mr-3" src={project.link.type === 'gitlab' ? Gitlab : Github}></img>
                            <a href={project.link.projectUrl || 'https://github.com/' + projectName  + '/' + project.name} className="inline text-sm font-medium tracking-wide lg:text-xsm">{projectName + '/' + project.name}</a>
                        </div>
                    </div>
                </div>
            </div>
            }) : <div></div>}
            </div>
        </div>
        </>
    )
 }
 
export default Mobile;