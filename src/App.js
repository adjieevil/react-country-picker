import React, {useEffect, useState} from 'react';
import {Helmet} from "react-helmet";
import Desktop from './components/desktop.js';
import Mobile from './components/mobile.js'
import { useMediaQuery } from 'react-responsive'

function App() {
  const isDesktopOrLaptop = useMediaQuery({
    query: '(min-device-width: 1224px)'
  })
  const isBigScreen = useMediaQuery({ query: '(min-device-width: 1824px)' })
  const isTabletOrMobile = useMediaQuery({ query: '(max-device-width: 1224px)' })

  return (
  <>
    <Helmet>
        <title>Country Picker</title>
        <meta name="description" content="Helmet application" />
    </Helmet>
      {isDesktopOrLaptop && <>
        <Desktop />
      </>}

      {isTabletOrMobile && <> 
        <Mobile />
      </>
      }

  </>
  );
}

export default App;
